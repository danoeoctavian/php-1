<?php

require ('animal.php');
require ('ape.php');
REQUIRE ('frog.php');

$sheep = new Animal('Shaun', '4', 'no');
echo "Nama : " . $sheep->nama;
echo "<br>";
echo "Legs : " . $sheep->legs;
echo "<br>";
echo "Cold Blooded : " . $sheep->cold_blooded;

echo "<br><br>";

$sungokong = new Ape('Kera Sakti', 2, 'no');
echo $sungokong->getInfoApe();

echo "<br><br>";

$kodok = new Frog ('Buduk', 4, 'no');
echo $kodok->getInfoFrog();


?>
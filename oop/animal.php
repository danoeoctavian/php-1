<?php

class Animal {
    public $nama, $legs, $cold_blooded, $jump, $yell;

    public function __construct($nama, $legs, $cold_blooded)
    {
        $this->nama = $nama;
        $this->legs = $legs;
        $this->cold_blooded = $cold_blooded;
    }
}

?>
<?php

class Ape extends Animal {
    public function __construct($nama, $legs = 2, $cold_blooded = "no")
    {
        parent::__construct($nama, $legs, $cold_blooded);
    }
    public function yell() {
        return "Auoo";
    }

    public function getInfoApe() {
        return "Nama : " . $this->nama . "<br>Legs : " . $this->legs . "<br>Cold Blooded : " . $this->cold_blooded . "<br>Yell : " .$this->yell();
    }
}

?>
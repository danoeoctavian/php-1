<?php

class Frog extends Animal {
    public function __construct($nama, $legs, $cold_blooded = "no")
    {
        parent::__construct($nama, $legs, $cold_blooded);
    }
    public function jump() {
        return "hop hop";
    }
    public function getInfoFrog() {
        return "Nama : " . $this->nama . "<br>Legs : " . $this->legs . "<br>Cold Blooded : " . $this->cold_blooded . "<br>Jump : " .$this->jump();
    }
}

?>